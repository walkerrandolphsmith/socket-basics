import socket
import select

class ChatServer:

	def __init__( self, port ):
		self.port = port;

		self.s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
		self.s.bind( ("", port) )
		self.s.listen( 10 )

		self.descriptors = [self.s]
		print 'ChatServer started on port %s' % port

	def run( self ):

		while 1:
			(sread, swrite, sexc) = select.select( self.descriptors, [], [] )

			for sock in sread:

				if sock == self.s:
					self.accept_new_connection()
				else:
					str = sock.recv(100)

					if str == '':
						host, port = sock.getpeername()
						str = 'Client left %s:%s\r\n' % (host, port)
						self.broadcast_string( str, sock )
						sock.close
						self.descriptors.remove(sock)
					else:
						host, port = sock.getpeername()
						newstr = '[%s:%s] %s' % (host, port, str)
						self.broadcast_string( newstr, sock )

	def accept_new_connection( self ):

		newsock, (remhost, remport) = self.s.accept()
		self.descriptors.append( newsock )

		newsock.send("You're connected to the Python chat server.\r\n")
		str = 'Client joined %s:%s\r\n' % (remhost, remport)
		self.broadcast_string( str, newsock )

	def broadcast_string( self, str, omit_sock ):

		for sock in self.descriptors:
			if sock != self.s and sock != omit_sock:
				sock.send(str)

		print str


server = ChatServer( 8888 ).run()